    SS2_G04

- 4 members: 
- Nguyễn Thu Hải
- Lê Mai Hương
- Nguyễn Trà My
- Lê Hồng Trang
             
- Topic: Image Enhancement

- Outline: 4 parts  
- I) Definition       
- II) Approaches for image enhancement
- III) Fundamental in Digital Image Processing (DIP)
- IV) Methods


